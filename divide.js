

var fs = require('fs');
var path = require('path');
//var dir = __dirname + "/corpus-20180608";
//var dir = __dirname + "/corpus-mio-jona";
//var dir = __dirname + "/corpus-final-colo-fix";
var dir = __dirname + "/corpus";



files = fs.readdirSync(dir, 'utf8').sort();

/*sorted = files.sort(function(a, b) {
    return path.parse(a).name = path.parse(b).name;
});*/

console.log("sorted: " + files);

var ids = [];

files.forEach(function(f) {
    if (ids.indexOf(f) == -1) {
        var id = f.split(".")[0];
        if (id != "")
            ids.push(id);
    }
})
console.log(ids.length);

//0 0 0 0 1 2
var train = [];
var test = [];
var valid = [];

function createFile(dir2, f){
    fs.createReadStream(dir + "/" + f + ".txt").pipe(fs.createWriteStream(__dirname +  dir2 + f + ".txt"));
    fs.createReadStream(dir + "/" + f + ".ann").pipe(fs.createWriteStream(__dirname +  dir2 + f + ".ann"));
}
var c = 0;
ids.forEach(function(id){
    if (c <= 3) {
        createFile("/train/", id);
        train.push(id);
        c++
    }else if (c == 4) {
        createFile("/test/", id);
        test.push(id);
        c++;
    }else if (c == 5) {
        createFile("/valid/", id);
        valid.push(id);
        c++;
    }else {
        createFile("/train/", id);
        train.push(id);
        c = 1;
    }
})

console.log(train.length, test.length, valid.length);
