T1	VENDOR 16 26	FrontRange
T2	PRODUCT 27 30	DSM
T3	VENDOR 143 153	FrontRange
T4	PRODUCT 154 157	DSM
T5	VENDOR 166 176	FrontRange
T6	VERSION 240 250	7.2.1.2020
T7	VERSION 253 263	7.2.2.2331
T8	VERSION 284 294	7.2.1.2020
T9	VERSION 297 307	7.2.2.2331
T10	PROBLEMTYPE 328 363	Use of Hard-coded Cryptographic Key
T11	PROBLEMTYPE 365 372	CWE-321
T12	PROBLEMTYPE 374 410	Insufficiently Protected Credentials
T13	PROBLEMTYPE 412 419	CWE-522
T14	PROBLEMTYPE 421 458	Violation of Secure Design Principles
T15	PROBLEMTYPE 460 467	CWE-657
T16	RISK_LEVEL 481 485	High
T22	DESCRIPTION 1504 2977	FrontRange DSM stores passwords for different user accounts encrypted in two configuration files named NiCfgLcl.ncp and NiCfgSrv.ncp. These configuration files contain encrypted password information for different required FrontRange DSM user accounts (see [2]), for example * DSM Runtime Service * DSM Distribution Service * Business Logic Server (BLS) Authentication * Database account A limited Windows domain user has read access to these configuration files that are usually stored in the following locations: * %PROGRAMFILES(X86)\NetInst\NiCfgLcl.ncp (local on a managed client) * %PROGRAMFILES(X86)\NetInst\NiCfgSrv.ncp (local on a managed client) * \\<FRONTRANGE SERVER>\DSM$\NiCfgLcl.ncp (remote on a DSM network share) * \\<FRONTRANGE SERVER>\DSM$\NiCfgSrv.ncp (remote on a DSM network share) The passwords are encoded and encrypted using a hard-coded secret (cryptographic key) contained within the FrontRange DSM executable file NIInst32.exe. The software solution FrontRange DSM insufficiently protects sensitive user credentials and violates secure design principles as limited user accounts have read access to the stored password information, the passwords can be recovered as cleartext using a hard-coded cryptographic key, and due to the software design the passwords are also used in the context of a low-privileged user process (NIInst32.exe) which can be analyzed and controlled by an attacker or malware running in the same low-privileged user context.
T23	VENDOR 3193 3203	FrontRange
T24	VENDOR 3326 3336	FrontRange
T25	PRODUCT 3337 3340	DSM
T26	VENDOR 3594 3604	FrontRange
T27	VENDOR 4626 4636	FrontRange
T28	PRODUCT 4637 4640	DSM
T29	VENDOR 4712 4722	FrontRange
T30	PRODUCT 4723 4726	DSM
T31	REFERENCES 4866 4948	https://www.syss.de/fileadmin/dokumente/Publikationen/Advisories/SYSS-2014-007.txt
T32	PROBLEMTYPE 4965 4985	Privilege Escalation
T17	DESCRIPTION 766 1399	The client management solution FrontRange Desktop & Server Management (DSM) stores and uses sensitive user credentials for required user accounts in an insecure manner which enables an attacker or malware with file system access to a managed client, for example with the privileges of a limited Windows domain user account, to recover the cleartext passwords. The recovered passwords can be used for privilege escalation attacks and for gaining unauthorized access to other client and/or server systems within the corporate network as at least one FrontRange DSM user account needs local administrative privileges on managed systems.
T18	REFERENCES 5018 5132	https://www.syss.de/fileadmin/dokumente/Publikationen/2015/Privilege_Escalation_via_Client_Management_Software.pdf
