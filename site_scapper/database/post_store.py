from database.mongo import MongoDB
import datetime

BUGTRAQ = 'bugtraq'
FULL_DISCLOSURE = 'full_disclosure'
EXPLOIT_DB = 'exploit_db'

DB_NAME = 'proy2017'
LINKS_COLLECTION_NAME = 'links'
POSTS_COLLECTION_NAME = 'posts'
CHOSEN_COLLECTION_NAME = 'chosen'

def save_link(site, year, month, link):
    database = MongoDB(DB_NAME)
    links = database.create_collection(LINKS_COLLECTION_NAME)
    post_info = {
        'site': site,
        'year': year,
        'month': month,
        'link': link,
        'downloaded': False,
        'error': False,
        'created': datetime.datetime.utcnow()
    }

    links.insert_one(post_info)


def save_link_exploit(site, link, title, author, platform):
    database = MongoDB(DB_NAME)
    links = database.create_collection(LINKS_COLLECTION_NAME)
    post_info = {
        'site': site,
        'link': link,
        'title': title,
        'author': author,
        'platform': platform,
        'downloaded': False,
        'error': False,
        'created': datetime.datetime.utcnow()
    }

    links.insert_one(post_info)


def get_links(site, year, month, downloaded=False, error=False):
    database = MongoDB(DB_NAME)
    links = database.create_collection(LINKS_COLLECTION_NAME)

    return links.find({
        'site': site,
        'year': year,
        'month': month,
        'downloaded': downloaded,
        'error': error
    })


def get_links_exploit(site, downloaded=False, error=False):
    database = MongoDB(DB_NAME)
    links = database.create_collection(LINKS_COLLECTION_NAME)

    return links.find({
        'site': site,
        'downloaded': downloaded,
        'error': error
    })


def mark_link(site, link):
    database = MongoDB(DB_NAME)
    links = database.create_collection(LINKS_COLLECTION_NAME)

    links.update_one({
        'site': site,
        'link': link
    }, {
        '$set': {
            'downloaded': True,
            'download_date': datetime.datetime.utcnow(),
            'error': False,
            'error_msg': ''

        }
    }, upsert=False)


def mark_error_link(site, link, error_msg):
    database = MongoDB(DB_NAME)
    links = database.create_collection(LINKS_COLLECTION_NAME)

    links.update_one({
        'site': site,
        'link': link
    }, {
        '$set': {
            'error': True,
            'error_msg': error_msg
        }
    }, upsert=False)


def save_post(site, report_link, title, from_who, date, content):
    post_info = {
        'site': site,
        'link': report_link,
        'title': title,
        'from': from_who,
        'date': date,
        'comments': [],
        'content': content,
        'created': datetime.datetime.utcnow()
    }

    save_post_data(post_info)


def save_post_data(post_data):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)

    posts.update_one({
        'site': post_data['site'],
        'link': post_data['link']
    }, {
        '$set': post_data
    }, upsert=True)


def add_comment_to_post(site, report_link, title, from_who, date, content, parent_link):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)
    comment_info = {
        'site': site,
        'link': report_link,
        'title': title,
        'from': from_who,
        'date': date,
        'content': content,
        'created': datetime.datetime.utcnow()
    }

    posts.update_one({
        'site': site,
        'link': parent_link
    }, {
        '$addToSet': {
            'comments': comment_info
        }
    }, upsert=False)


def count_seclist_posts(site, year):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)
    link_filter = '/' + str(year) + '.*'

    return posts.find({
        'site': site,
        'link': {'$regex': link_filter}
    }).count()


def count_exploitdb_posts(site, year):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)
    date_filter = '^' + str(year) + '.*'

    return posts.find({
        'site': site,
        'date': {'$regex': date_filter}
    }).count()


def get_posts_sample(site, year, sample_count):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)
    link_filter = '/' + str(year) + '.*'
    date_filter = '^' + str(year) + '.*'

    if site == EXPLOIT_DB:
        return posts.aggregate([
            {'$match': {'$and': [{'site': site}, {'date': {'$regex': date_filter}}]}},
            {'$sample': {'size': sample_count}}
        ])
    else:
        return posts.aggregate([
            {'$match': {'$and': [{'site': site}, {'link': {'$regex': link_filter}}]}},
            {'$sample': {'size': sample_count}}
        ])


def copy_chosen_samples(samples):
    database = MongoDB(DB_NAME)
    chosen = database.create_collection(CHOSEN_COLLECTION_NAME)

    chosen.insert(samples)


def delete_chosen_from_posts():
    database = MongoDB(DB_NAME)
    chosen = database.create_collection(CHOSEN_COLLECTION_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)

    selected_ones = chosen.find({})
    for so in selected_ones:
        # print(so['link'])
        posts.remove({'link': so['link']})


def get_cve_posts_exploit(year):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)
    date_filter = '^' + str(year) + '.*'

    return posts.find({
        'site': EXPLOIT_DB,
        'cve': {'$nin': ['']},
        'date': {'$regex': date_filter}
    })


def get_cve_posts_seclists(site, year):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)
    start = datetime.datetime(year, 1,  1,  0,  0,  0, 0)
    end = datetime.datetime(year, 12, 31, 23, 59, 59, 0)
    #db.wing_model.find({'time': {'$gte': start, '$lt': end}})

    return posts.find({
        'site': site,
        'date': {'$gte': start, '$lt': end}
    })
