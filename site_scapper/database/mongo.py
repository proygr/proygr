import pymongo


class MongoDB:

    def __init__(self, dbname):
        self._client = pymongo.MongoClient('localhost', 27017)
        self._database = self._client[dbname]

    def create_collection(self, name=""):
        return self._database[name]

