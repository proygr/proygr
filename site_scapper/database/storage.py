from database.mongo import MongoDB
import datetime

BUGTRAQ = 'bugtraq'
FULL_DISCLOSURE = 'full_disclosure'
EXPLOIT_DB = 'exploit_db'

DB_NAME = 'vulnerabilidades'
LINKS_COLLECTION_NAME = 'links'
POSTS_COLLECTION_NAME = 'posts'
CHOSEN_COLLECTION_NAME = 'chosen'


# site, year, month, link
# site, link, title, author, platform
def save_link(link_filter, link_info):
    database = MongoDB(DB_NAME)
    links = database.create_collection(LINKS_COLLECTION_NAME)
    new_link_info = dict(link_info)
    new_link_info.update({
        'downloaded': False,
        'error': False,
        'created': datetime.datetime.utcnow()
    })

    links.update_one(link_filter, {
        '$set': new_link_info
    }, upsert=True)


# site, year, month
# site
def get_links(links_filter, downloaded=None, error=None):
    database = MongoDB(DB_NAME)
    links = database.create_collection(LINKS_COLLECTION_NAME)

    new_filter = dict(links_filter)
    if downloaded is not None:
        new_filter.update({
            'downloaded': downloaded
        })
    if error is not None:
        new_filter.update({
            'error': error
        })

    return links.find(new_filter)


# site, link
def mark_link(link_filter):
    database = MongoDB(DB_NAME)
    links = database.create_collection(LINKS_COLLECTION_NAME)

    links.update_one(link_filter, {
        '$set': {
            'downloaded': True,
            'download_date': datetime.datetime.utcnow(),
            'error': False,
            'error_msg': ''

        }
    }, upsert=False)


# site, link
def mark_error_link(link_filter, error_msg):
    database = MongoDB(DB_NAME)
    links = database.create_collection(LINKS_COLLECTION_NAME)

    links.update_one(link_filter, {
        '$set': {
            'error': True,
            'error_msg': error_msg
        }
    }, upsert=False)


# site, report_link, title, from_who, date, content
def save_post(post_info):
    new_post_info = dict(post_info)
    new_post_info.update({
        'created': datetime.datetime.utcnow()
    })

    save_post_data(new_post_info)


def save_post_data(post_filter, post_data):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)

    posts.update_one(post_filter, {
        '$set': post_data
    }, upsert=True)


def add_comment_to_post(site, report_link, title, from_who, date, content, parent_link):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)
    comment_info = {
        'site': site,
        'link': report_link,
        'title': title,
        'from': from_who,
        'date': date,
        'content': content,
        'created': datetime.datetime.utcnow()
    }

    posts.update_one({
        'site': site,
        'link': parent_link
    }, {
        '$addToSet': {
            'comments': comment_info
        }
    }, upsert=False)


def count_seclist_posts(site, year):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)
    link_filter = '/' + str(year) + '.*'

    return posts.find({
        'site': site,
        'link': {'$regex': link_filter}
    }).count()


def count_exploitdb_posts(site, year):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)
    date_filter = '^' + str(year) + '.*'

    return posts.find({
        'site': site,
        'date': {'$regex': date_filter}
    }).count()


def get_posts_sample(site, year, sample_count):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)
    link_filter = '/' + str(year) + '.*'
    date_filter = '^' + str(year) + '.*'

    if site == EXPLOIT_DB:
        return posts.aggregate([
            {'$match': {'$and': [{'site': site}, {'date': {'$regex': date_filter}}]}},
            {'$sample': {'size': sample_count}}
        ])
    else:
        return posts.aggregate([
            {'$match': {'$and': [{'site': site}, {'link': {'$regex': link_filter}}]}},
            {'$sample': {'size': sample_count}}
        ])


def copy_chosen_samples(samples):
    database = MongoDB(DB_NAME)
    chosen = database.create_collection(CHOSEN_COLLECTION_NAME)

    chosen.insert(samples)


def delete_chosen_from_posts():
    database = MongoDB(DB_NAME)
    chosen = database.create_collection(CHOSEN_COLLECTION_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)

    selected_ones = chosen.find({})
    for so in selected_ones:
        # print(so['link'])
        posts.remove({'link': so['link']})


def get_cve_posts_exploit(year):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)
    date_filter = '^' + str(year) + '.*'

    return posts.find({
        'site': EXPLOIT_DB,
        'cve': {'$nin': ['']},
        'date': {'$regex': date_filter}
    })


def get_posts_from_date_exploit(from_date):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)
    date_filter = '^' + from_date

    return posts.find({
        'site': EXPLOIT_DB,
        'date': {'$gte': date_filter}
    })


def get_cve_posts_seclists(site, year):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)
    start = datetime.datetime(year, 1,  1,  0,  0,  0, 0)
    end = datetime.datetime(year, 12, 31, 23, 59, 59, 0)

    return posts.find({
        'site': site,
        'date': {'$gte': start, '$lt': end}
    })


def get_posts_from_date_seclists(site, from_date):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)

    return posts.find({
        'site': site,
        'date': {'$gte': from_date}
    })


def get_posts_from_date(site, from_date):
    database = MongoDB(DB_NAME)
    posts = database.create_collection(POSTS_COLLECTION_NAME)

    return posts.find({
        'site': site,
        'date': {'$gte': from_date}
    })

