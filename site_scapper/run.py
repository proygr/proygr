from __future__ import unicode_literals
import os
from flask import Flask, jsonify, send_from_directory
from flask_cors import CORS
from scrappers.exploit_scrapper import exploit_scrapper as exploitCtrl
from scrappers.seclists_scrapper import seclists_scrapper as seclistCtrl
from predictor.neuroner_connector import neuroner_connector as neuronerCtrl
from predictor.crf_connector import crf_connector as crfCtrl

app = Flask(__name__)
cors = CORS(app)

@app.route('/app/<path:path>')
def root(path):
  return send_from_directory('app', path)


@app.errorhandler(404)
def not_found(error):
    return jsonify(error=str(error)), 404


@app.errorhandler(Exception)
def handle_error(e):
    code = 500
    return jsonify(error=str(e)), code


def initialize_app(flask_app):
    flask_app.register_blueprint(exploitCtrl)
    flask_app.register_blueprint(seclistCtrl)
    flask_app.register_blueprint(neuronerCtrl)
    flask_app.register_blueprint(crfCtrl)
    


if __name__ == "__main__":
    initialize_app(app)
    port = 7082
    app.path = os.path.abspath(os.path.dirname(__file__))
    app.run(host='0.0.0.0', port=port, threaded=True, debug=True)
