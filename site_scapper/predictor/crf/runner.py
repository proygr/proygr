import numpy as np
import pycrfsuite
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix
from crf.annotation_reader import read_all_files, extract_features, get_labels
from datetime import datetime


# test_ann_path = 'C:\\Users\\Jonatan\\Desktop\\proyecto\\data\\20180608-all\\35691.ann'
# test_text_path = 'C:\\Users\\Jonatan\\Desktop\\proyecto\\data\\20180608-all\\35691.txt'
# C:\\Users\\Jonatan\\Desktop\\proyecto\\data\\20180608-all
# C:\\Users\\Jonatan\\Desktop\\proyecto\\data\\corpus-naty-20180720
# C:\\Users\\Jonatan\\Desktop\\proyecto\\data\\corpus-final-jona-fix-naty2\\corpus-jona
# C:\\Users\\Jonatan\\Desktop\\proyecto\\data\\corpus-final-colo-fix
# D:\\fing\\proyecto\\data\\todo
# 'C:\\Users\\Jonatan\\Desktop\\proyecto\\data\\20181021-all'
print('start:', datetime.now())
test_source_path = 'D:\\fing\\proyecto\\data\\todo'

# res = read_ann_file(test_ann_path, test_text_path)
res = read_all_files(test_source_path)
# print('total: ', len(res))

X = [extract_features(doc) for doc in res]
y = [get_labels(doc) for doc in res]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

trainer = pycrfsuite.Trainer(verbose=True)

# Submit training data to the trainer
for xseq, yseq in zip(X_train, y_train):
    trainer.append(xseq, yseq)

# Set the parameters of the model
trainer.set_params({
    # coefficient for L1 penalty
    'c1': 0.1,

    # coefficient for L2 penalty
    'c2': 0.01,

    # maximum number of iterations
    'max_iterations': 200,

    # whether to include transitions that
    # are possible, but not observed
    'feature.possible_transitions': True
})

# Provide a file name as a parameter to the train function, such that
# the model will be saved to the file when training is finished
# examples used: crf_jona.model, crf_naty.model
model_name = 'crf_todo.model'
trainer.train(model_name)

tagger = pycrfsuite.Tagger()
tagger.open(model_name)
y_pred = [tagger.tag(xseq) for xseq in X_test]

# Let's take a look at a random sample in the testing set
# i = 12
# for x, y in zip(y_pred[i], [x[1].split("=")[1] for x in X_test[i]]):
#     print("%s (%s)" % (y, x))

# Create a mapping of labels to indices
labels = {
        "NOT_LABELED": 0, "VENDOR": 1, "PRODUCT": 2,
        "VERSION": 3, "PROBLEMTYPE": 4, "REFERENCES": 5,
        "DESCRIPTION": 6, "AFFECTEDPRODUCTS": 7,
        "RISK_LEVEL": 8, "BASE_SCORE": 9, "EXPLOITABLE_FROM": 10,
        "DIFFICULTY": 11
}

# Convert the sequences of tags into a 1-dimensional array
labels_set = set()
[labels_set.add(tag) for row in y_pred for tag in row]
print('labels used:', labels_set)
predictions = np.array([labels[tag] for row in y_pred for tag in row])
truths = np.array([labels[tag] for row in y_test for tag in row])

label_names = ["NOT_LABELED", "VENDOR", "PRODUCT",
               "VERSION", "PROBLEMTYPE", "REFERENCES",
               "DESCRIPTION",
               "RISK_LEVEL", "BASE_SCORE", "EXPLOITABLE_FROM", "DIFFICULTY"]

# Print out the classification report
# target_names=label_names
print(classification_report(
    truths, predictions, target_names=label_names, labels=[0,1,2,3,4,5,6,8,9,10,11]))

print(confusion_matrix(truths, predictions))

print('end:', datetime.now())
