import pycrfsuite
import nltk
import mmap
from predictor.crf.annotation_reader import extract_features



def _crf_tag_file(file_path):
    tagger = pycrfsuite.Tagger()
    tagger.open("predictor/crf/crf_todo.model")
    words = []

    with open(file_path, 'r', encoding='utf-8') as text_file:
        
        for text_line in text_file:
            for word in text_line.split():
                words.append(word)

    tagged = nltk.pos_tag(words)
    data = [tagged]

    features = extract_features(data)
    return tagger.tag(features), tagged


'''
    Recibe un archivo de texto con el post, usamos CRF para etiquetar
    las palabras y retornamos un string con las anotaciones en formato .ann
'''


def predict_file(file_path):

    pred, tagged = _crf_tag_file(file_path)
    with open(file_path, 'r+', encoding='utf-8') as text_file:
        mapped_file = mmap.mmap(text_file.fileno(), 0)
        results = ''
        current_label = ''
        sum_text = ''
        current_index = 0
        current_ann_index = 1

        for x, y in zip([x[0] for x in tagged], pred):
            
            if current_label != y and sum_text != '':
                index = mapped_file.find(sum_text.encode(), current_index)
                current_index = index + len(sum_text)
                # print('T' + str(current_ann_index), current_label, index + 1, str(current_index) + '\t' + sum_text)
                results = results + 'T' + str(current_ann_index) + ' ' + current_label \
                          + ' ' + str(index + 1) + ' ' + str(current_index) + '\t' + sum_text + '\n'

                sum_text = ''
                current_ann_index += 1

            if current_label != y:
                current_label = y

            if current_label != 'NOT_LABELED':
                if sum_text == '':
                    sum_text = x
                else:
                    sum_text = sum_text + ' ' + x

    return results


def test():
    annotations = predict_file('D:\\fing\\proyecto\\data\\todo\\5a12c8bf5b60c30f5449f46b.txt')
    print(annotations)


if __name__ == "__main__":
    print('crf_predictor main')
    test()
