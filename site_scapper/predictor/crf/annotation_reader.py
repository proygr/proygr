from os import listdir
import nltk
import pycrfsuite
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.metrics import classification_report, confusion_matrix


class LabeledText:
    def __init__(self, start, end, label):
        self.start = start
        self.end = end
        self.label = label

    def inside(self, a, b):
        result = self.end >= b and b >= a and a >= self.start
        return result


def word2features(doc, i):
    word = doc[i][0]
    postag = doc[i][1]

    # Common features for all words
    features = [
        'bias',
        'word.lower=' + word.lower(),
        'word[-3:]=' + word[-3:],
        'word[-2:]=' + word[-2:],
        'word[1:]=' + word[1:],
        'word[1:-1]=' + word[1:-1],
        'word.isupper=%s' % word.isupper(),
        'word.istitle=%s' % word.istitle(),
        'word.isdigit=%s' % word.isdigit(),
        'postag=' + postag
    ]

    # Features for words that are not
    # at the beginning of a document
    if i > 0:
        word1 = doc[i-1][0]
        postag1 = doc[i-1][1]
        features.extend([
            '-1:word.lower=' + word1.lower(),
            '-1:word.istitle=%s' % word1.istitle(),
            '-1:word.isupper=%s' % word1.isupper(),
            '-1:word.isdigit=%s' % word1.isdigit(),
            '-1:postag=' + postag1
        ])
    else:
        # Indicate that it is the 'beginning of a document'
        features.append('BOS')

    if i > 1:
        word2 = doc[i-2][0]
        postag2 = doc[i-2][0]
        features.extend([
            '-2:word.lower=' + word2.lower(),
            '-2:word.istitle=%s' % word2.istitle(),
            '-2:word.isupper=%s' % word2.isupper(),
            '-2:word.isdigit=%s' % word2.isdigit(),
            '-2:postag=' + postag2
        ])
    
    # Features for words that are not
    # at the end of a document
    if i < len(doc)-1:
        word1 = doc[i+1][0]
        postag1 = doc[i+1][1]
        features.extend([
            '+1:word.lower=' + word1.lower(),
            '+1:word.istitle=%s' % word1.istitle(),
            '+1:word.isupper=%s' % word1.isupper(),
            '+1:word.isdigit=%s' % word1.isdigit(),
            '+1:postag=' + postag1
        ])
    else:
        # Indicate that it is the 'end of a document'
        features.append('EOS')

    return features


def find_label(label_refs, start, end):
    for lr in label_refs:
        if lr.inside(start, end):
            return lr.label

    return ''


def read_ann_file(ann_file_path, text_file_path):
    results = []

    with open(ann_file_path, 'r', encoding='utf-8') as ann_file, open(text_file_path, 'r', encoding='utf-8') as text_file:
        # contents = text_file.read()
        label_refs = []

        for ann_line in ann_file:
            words = ann_line.split()
            gold_label = words[1]
            start_index = words[2]
            end_index = words[3]

            lt = LabeledText(int(start_index), int(end_index)-1, gold_label)
            label_refs.append(lt)

        total_index = 0
        for text_line in text_file:
            print('line:', total_index)
            for word in text_line.split():
                start_index = total_index
                end_index = start_index + len(word) - 1
                label = find_label(label_refs, start_index, end_index)

                if label != '':
                    results.append((word, label))
                else:
                    results.append((word, 'NOT_LABELED'))

                total_index = total_index + len(word) + 1
            total_index += 1

        # print('results', results)
    print('words: ', ann_file_path, len(results))

    tokens = [t for t, label in results]
    tagged = nltk.pos_tag(tokens)
    data = [[(w, pos, label) for (w, label), (word, pos) in zip(results, tagged)]]

    return data


def read_all_files(source_path):
    results = []
    for file in listdir(source_path):
        ann_path = source_path + '\\' + file
        if file.endswith('.ann'):
            post_name = file.split('.')[0] + '.txt'
            post_path = source_path + '\\' + post_name
            parsed = read_ann_file(ann_path, post_path)
            results.append(parsed)

    return results


# A function for extracting features in documents
def extract_features(doc):
    return [word2features(doc[0], i) for i in range(len(doc[0]))]


# A function fo generating the list of labels for each document
def get_labels(doc):
    return [label for (token, postag, label) in doc[0]]
