var fs = require('fs');
var folder = __dirname + "/NeuroNER/output";
var cp = require('child_process');
var path = require('path');
var os = require("os");




var url = "mongodb://127.0.0.1:27017/cves";
var MongoClient = require('mongodb').MongoClient;

if (process.argv[2] == "crf") {
    folder = __dirname + "/crf/output";
}

var collectionName = process.argv[3];

cp.exec("cd " + folder + " && ls", function(err, out){
    var outdir = "";

    console.log(process.argv[2])
    if (process.argv[2] == "crf") {
        outdir = folder;
    } else {
        outdir = folder + "/" + out.trim() + "/brat/deploy";
    }

    console.log(outdir);

    files = fs.readdirSync(outdir, 'utf8');

    var cves = [];

    console.log(files);
    for (let file of files) {
        if (path.extname(file) == '.ann') {
            var map = {};
            
            console.log(file);
            var data = fs.readFileSync(outdir + "/" + file, {encoding: 'utf-8'});
            console.log(data);
            lines = data.split(os.EOL);
            for (let line of lines) {
                var cleanedLine = line.replace(/^T\d+\s+/, "");
                cleanedLine = cleanedLine.replace(/^([A-Z|-]+)\s+\d+\s+\d+/, "$1");
                console.log(cleanedLine);

                var tag = cleanedLine.match(/^[A-Z|-]+/) + "";
                //console.log("tag: " + tag);
                if (tag != 'null') {
                    var value = cleanedLine.substr(tag.length, cleanedLine.length).trim();
                    //console.log("value: " + value);
                    if (map[tag] != undefined) {
                        //console.log("has tag: ", map[tag], typeof(map[tag]));
                        
                        var hasItem = false;
                        for (let c of map[tag]) {
                            if (c.toLowerCase() == value.toLowerCase()) {
                                hasItem = true;
                                break;
                            }
                        }
                        if (!hasItem){
                            map[tag].push(value);
                        }
                        //map.set(tag, map.get(tag).push(value));
                    } else {
                        //console.log("new tag");
                        map[tag] = [value];
                    }
                    //console.log(map[tag]);
                }
            }
            console.log(map);

            var json = {
                "data_type": "CVE",
                "data_format": "MITRE",
                "data_version": "4.0",
                "affects": {
                    "vendor": {
                        "vendor_data": [
                        ]
                    }
                },
                "problemtype":{
                    "problemtype_data":[
                        
                    ]
                },
                "references":{
                    "reference_data":[
                        
                    ]
                },
                "description":{
                    "description_data":[
                    ]
                }
            };

            if (map["VENDOR"] != undefined) {
                map["VENDOR"].forEach(function(vendor){
                    var vendorAux = {
                        "vendor_name": vendor,
                        "product": {
                            "product_data": [
                            ]
                        }
                    };
                    if (map["PRODUCT"] != undefined) {
                        map["PRODUCT"].forEach(function(product, index){
                            var productAux = {
                                "product_name": product,
                                "version": {
                                    "version_data": [
                                    ]
                                }
                            };
                            
                            if (map["VERSION"] && map["VERSION"].length == 1){
                                productAux.version.version_data.push({
                                    "version_value": map["VERSION"][0]
                                });
                            }else if (map["VERSION"] && map["VERSION"][index]){
                                productAux.version.version_data.push({
                                    "version_value": map["VERSION"][index]
                                });
                            }
                            vendorAux.product.product_data.push(productAux);
                        });
                    } else {
                        var productAux = {
                            "product_name": "-",
                            "version": {
                                "version_data": [
                                ]
                            }
                        };
                        
                        if (map["VERSION"] != undefined) {
                            map["VERSION"].forEach(function(version){
                                productAux.version.version_data.push({
                                    "version_value": version
                                });
                                
                            });
                        } 
                        vendorAux.product.product_data.push(productAux);
                        
                    }
                    json.affects.vendor.vendor_data.push(vendorAux);
                });
            } else {
                
                var vendorAux = {
                    "vendor_name": "-",
                    "product": {
                        "product_data": [
                        ]
                    }
                };
                
                if (map["PRODUCT"] != undefined) {
                    map["PRODUCT"].forEach(function(product, index){
                        var productAux = {
                            "product_name": product,
                            "version": {
                                "version_data": [
                                ]
                            }
                        };
                        if (map["VERSION"] && map["VERSION"].length == 1){
                            productAux.version.version_data.push({
                                "version_value": map["VERSION"][0]
                            });
                        } else if (map["VERSION"] && map["VERSION"][index]){
                            productAux.version.version_data.push({
                                "version_value": map["VERSION"][index]
                            });
                        }
                        vendorAux.product.product_data.push(productAux);
                    });
                } else {
                    var productAux = {
                        "product_name": "-",
                        "version": {
                            "version_data": [
                            ]
                        }
                    };

                    if (map["VERSION"] != undefined) {
                        map["VERSION"].forEach(function(version){
                            productAux.version.version_data.push({
                                "version_value": version
                            });
                            
                        });
                    } 
                    vendorAux.product.product_data.push(productAux);
                    
                    
                    /*if (map["VERSION"] && map["VERSION"].length == 1){
                        productAux.version.version_data.push({
                            "version_value": map["VERSION"][0]
                        });
                    } 
                    vendorAux.product.product_data.push(productAux);*/
                }
                json.affects.vendor.vendor_data.push(vendorAux);
            }

            if (map["PROBLEMTYPE"] != undefined) {
                map["PROBLEMTYPE"].forEach(function(problemtype){
                    var problemtypeAux = {
                        "description":[
                            {
                                "lang": "en",
                                "value": problemtype
                            }
                        ]
                    };

                    json.problemtype.problemtype_data.push(problemtypeAux);
                });

                
            }

            if (map["REFERENCES"] != undefined) {
                map["REFERENCES"].forEach(function(references){
                    var referencesAux = {
                        "url":references
                    };

                    json.references.reference_data.push(referencesAux);
                });

                
            }

            if (map["DESCRIPTION"] != undefined) {
                var desc = "";
                map["DESCRIPTION"].forEach(function(description){

                    desc += description;
                    
                });

                var descriptionAux = {
					"lang": "en",
					"value": desc 
				}

                json.description.description_data.push(descriptionAux);

                
            }
            
            cves.push(json);
            console.log(JSON.stringify(json, "\t", 2));


        }
        
    }

    MongoClient.connect(url, function(err, client) {
        console.log("Connected successfully to server");
        var db = client.db("cves");
        var collection = db.collection(collectionName);
        // Find some documents
        collection.insert(cves);
        client.close();
      });
    });

