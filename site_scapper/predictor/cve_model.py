

def create_cve():
    return {
        "CVE_data_type": "CVE",
        "CVE_data_format": "MITRE",
        "CVE_data_version": "4.0",
        "CVE_data_numberOfCVEs": "0",
        "CVE_data_timestamp": "",
        "CVE_Items": []
    }


def add_cve_item(cve_model, cve_item):
    cve_model['CVE_Items'].insert(cve_item)


def create_cve_item():
    return {
        "cve": {
            "data_type": "CVE",
            "data_format": "MITRE",
            "data_version": "4.0",
            "CVE_data_meta": {
            "ID": "",
            "ASSIGNER": ""
            },
            "affects": {
                "vendor": {
                    "vendor_data": []
                }
            },
            "problemtype": {
                "problemtype_data": [{
                    "description": []
                }]
            },
            "references": {
                "reference_data": []
            },
            "description": {
                "description_data": []
            }
        },
        "configurations": {},
        "impact": {},
        "publishedDate": "",
        "lastModifiedDate": ""
    }


def add_cve_item_desc(cve_item, desc):
    cve_item['cve']['description']['description_data'].insert({
        "lang": "en",
        "value": desc
    })


def add_cve_item_problem_type(cve_item, problem_type):
    cve_item['cve']['problemtype']['problemtype_data'][0]["description"].insert({
        "lang": "en",
        "value": problem_type
    })


def add_cve_item_ref(cve_item, ref):
    cve_item['cve']['references']['reference_data'].insert({
        "url": ref,
        "name": ref,
        "refsource": "",
        "tags": []
    })


def add_cve_item_vendor(cve_item, vendor_name, products_data):
    products = []
    for p in products_data:
        versions = []
        for v in p.versions:
            version_item = {
                "version_value": v.value,
                "version_affected": v.affected
            }
            versions.insert(version_item)
        product_item = {
            "product_name": p.name,
            "version": {
                "version_data": versions
            }
        }
        products.insert(product_item)

    vendor_item = {
        "vendor_name": vendor_name,
        "product": {
            "product_data": products
        }
    }

    cve_item['cve']['affects']['vendor']['vendor_data'].insert(vendor_item)
