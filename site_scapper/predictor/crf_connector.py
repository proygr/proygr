from database.storage import get_posts_from_date, EXPLOIT_DB, BUGTRAQ, FULL_DISCLOSURE
from predictor.crf.crf_predictor import predict_file
import subprocess
import os
from datetime import datetime
from flask import Blueprint, jsonify, request

crf_connector = Blueprint('crf_connector_blueprint', __name__,
                 url_prefix='/crf')


CRF_DEPLOY_PATH = os.path.dirname(__file__) + '/crf/deploy'
CRF_OUTPUT_PATH = os.path.dirname(__file__) + '/crf/output'



@crf_connector.errorhandler(Exception)
def handle_error(e):
    code = 400
    print(e)
    return jsonify(error=str(e)), code

@crf_connector.route("/predict/<site>/<from_date>")
def predict(site, from_date):
    print(from_date)
    
    from_date = datetime.strptime(from_date, '%d-%m-%Y')

    command = 'rm -r ' + CRF_DEPLOY_PATH + ' && rm -r ' + CRF_OUTPUT_PATH
    subprocess.call(command, shell=True)

    command = 'mkdir ' + CRF_DEPLOY_PATH + ' && mkdir ' + CRF_OUTPUT_PATH
    subprocess.call(command, shell=True)

    

    # extraemos los posts de la base de datos y los ponemos en una carpeta
    posts_from_db = get_posts_from_date(site, from_date)
    
    for post in posts_from_db:
        # borramos los saltos de linea
        content = post['content'].replace('\r\n', ' ')
        content = " ".join(content.split())
        content = post['title'] + '\n' + content
        file_name = str(post['_id']) + '.txt'
        file_path = os.path.join(CRF_DEPLOY_PATH, file_name)

        with open(file_path, 'w', encoding='utf-8') as file:
            file.write(content)
            file.close()

    for post_file in os.listdir(CRF_DEPLOY_PATH):
        if post_file.endswith('.txt'):
            post_path = os.path.join(CRF_DEPLOY_PATH, post_file)
            post_ann_name = post_file.split('.')[0] + '.ann'
            post_ann_path = os.path.join(CRF_OUTPUT_PATH, post_ann_name)
            annotations = predict_file(post_path)
            with open(post_ann_path, 'w', encoding='utf-8') as post_ann_file:
                post_ann_file.write(annotations)
                post_ann_file.close()

    print('start with output processing...')
    
    main_path = os.path.join(os.path.dirname(__file__), "ann2cve.js")
    subprocess.call(["node", main_path, "crf", site + "-" + from_date.strftime('%Y-%m-%d')])
    return jsonify('{"data":"200 OK"}')


def test():
    from_date = datetime.strptime('30/11/2018', '%d/%m/%Y')
    predict(BUGTRAQ, from_date, 'D:\\fing\\proyecto\\data\\test')


if __name__ == "__main__":
    test()
