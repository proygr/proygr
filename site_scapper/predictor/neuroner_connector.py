import subprocess
import os
from flask import Blueprint, jsonify, request
from database.storage import get_posts_from_date
from datetime import datetime
#from flask import Flask, jsonify
#from flask_cors import CORS


#app = Flask(__name__)
#cors = CORS(app)

neuroner_connector = Blueprint('neuroner_connector_blueprint', __name__,
                 url_prefix='/neuroner')

@neuroner_connector.errorhandler(Exception)
def handle_error(e):
    code = 400
    print(e)
    return jsonify(error=str(e)), code


NEURONER_PATH = 'predictor/NeuroNER'
DATABASE_PATH = 'database'


def find_last_folder():
    result = ''
    output_path = os.path.join(NEURONER_PATH, 'output')
    for out_dir in os.listdir(output_path):
        if result < out_dir:
            result = out_dir

    print(result)
    return result


def process_files():
    folder_path = find_last_folder()
    for file in os.listdir(folder_path):
        file_path = os.path.join(folder_path, file)
        if file.endswith('.ann') and (os.getsize(file_path) > 0):
            print(file)


folder = os.path.dirname(__file__) + "/NeuroNER/data/dataset/en/deploy"
folder_en = os.path.dirname(__file__) + "/NeuroNER/data/dataset/en"
folder_output = os.path.dirname(__file__) + "/NeuroNER/output"

@neuroner_connector.route("/predict/<site>/<from_date>")
def predict(site, from_date):
    print('starting prediction engine')

    from_date = datetime.strptime(from_date, '%d-%m-%Y')
    
    command = "rm -r " + folder_en + ' && mkdir ' + folder_en + ' && mkdir ' + folder
    subprocess.call(command, shell=True)

    command = "rm -r " + folder_output + ' && mkdir ' + folder_output
    subprocess.call(command, shell=True)

    

    # extraemos los posts de la base de datos y los ponemos en una carpeta
    posts_from_db = get_posts_from_date(site, from_date)
    
    for post in posts_from_db:
        # borramos los saltos de linea
        content = post['content'].replace('\r\n', ' ')
        content = " ".join(content.split())
        content = post['title'] + '\n' + content
        file_name = str(post['_id']) + '.txt'
        file_path = os.path.join(folder, file_name)

        with open(file_path, 'w', encoding='utf-8') as file:
            file.write(content)
            file.close()
    

    main_path = os.path.join(NEURONER_PATH, 'src')
    
    command = 'cd ' + main_path + ' && python main.py'
    subprocess.call(command, shell=True)
    
    print('start with output processing...')
    
    main_path = os.path.join(os.path.dirname(__file__), "ann2cve.js")
    subprocess.call(["node", main_path, "neuroner", site + "-" + from_date.strftime('%Y-%m-%d')])
    return jsonify('{"data":"200 OK"}')


def initialize_app(flask_app):
    flask_app.register_blueprint(seclistCtrl)
    flask_app.register_blueprint(exploitCtrl)
    

    db.init_app(flask_app)
    with app.app_context():
        db.engine.echo = True
        db.metadata.bind = db.engine
        #db.drop_all() #uncomment to drop database
        db.metadata.create_all(checkfirst=True)

#predict()
#if __name__ == "__main__":
#   initialize_app(app)
#    port = 7080
#    app.path = os.path.abspath(os.path.dirname(__file__))
#    app.run(host='0.0.0.0', port=port, threaded=True, debug=True)


