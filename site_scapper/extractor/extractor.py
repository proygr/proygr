from database.post_store import get_cve_posts_exploit, get_cve_posts_seclists
from database.post_store import BUGTRAQ, FULL_DISCLOSURE
from os.path import join
from os import remove
import traceback


def create_files(year, dir_path, trimmed=False):
    posts = get_cve_posts_exploit(year)
    print('count:', posts.count())

    for post in posts:
        text_file_name = post['edb_id'] + '.txt'
        text_file_name = join(dir_path, text_file_name)
        ann_file_name = post['edb_id'] + '.ann'
        ann_file_name = join(dir_path, ann_file_name)

        #print('text_file_name:', text_file_name)
        #print('ann_file_name:', ann_file_name)

        file = open(text_file_name, 'w', encoding='utf-8')
        try:
            content = post['title'] + '\n' + post['content']
            if trimmed:
                content = post['content'].replace('\r\n', ' ')
                content = " ".join(content.split())
                content = post['title'] + '\n' + content

            file.write(content)
            file.close()
            file = open(ann_file_name, 'w')
            file.close()
        except:
            print(traceback.format_exc())
            file.close()
            remove(text_file_name)


def create_files_seclists(site, year, dir_path, trimmed=False):
    posts = get_cve_posts_seclists(site, year)
    print('count:', posts.count())

    for post in posts:
        prefix = get_file_name_seclists(post['link'])
        text_file_name = prefix + '.txt'
        text_file_name = join(dir_path, text_file_name)
        ann_file_name = prefix + '.ann'
        ann_file_name = join(dir_path, ann_file_name)

        file = open(text_file_name, 'w', encoding='utf-8')
        try:
            content = post['title'] + '\n' + post['content']
            if trimmed:
                content = post['content'].replace('\r\n', ' ')
                content = " ".join(content.split())
                content = post['title'] + '\n' + content

            file.write(content)
            file.close()
            file = open(ann_file_name, 'w')
            file.close()
        except:
            print(traceback.format_exc())
            file.close()
            remove(text_file_name)


def get_file_name_seclists(link):
    splitted = link.split('/')
    size = len(splitted)
    return splitted[size-3] + '_' + splitted[size-2] + '_' + splitted[size-1]


create_files(2016, 'C:\\Users\\Jonatan\\Desktop\\proyecto\\data\\exploit2016-trim', True)
# create_files_seclists(BUGTRAQ, 2015, 'C:\\Users\\Jonatan\\Desktop\\proyecto\\bugtraq2015-trim', True)
# create_files_seclists(FULL_DISCLOSURE, 2015, 'C:\\Users\\Jonatan\\Desktop\\proyecto\\fulldisclosure2015-trim', True)
