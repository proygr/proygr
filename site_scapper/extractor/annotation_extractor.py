from os import listdir
from os.path import getsize
import shutil

def copy(source_path, dest_path):
    count = 0
    for file in listdir(source_path):
        ann_original_path = source_path + '\\' + file
        if file.endswith('.ann') and (getsize(ann_original_path) > 0):
            count += 1
            post_name = file.split('.')[0] + '.txt'
            post_original_path = source_path + '\\' + post_name
            ann_dest_path = dest_path + '\\' + file
            post_dest_path = dest_path + '\\' + post_name

            shutil.copy(ann_original_path, ann_dest_path)
            shutil.copy(post_original_path, post_dest_path)

    print('count', count)


dest_path = 'C:\\Users\\Jonatan\\Desktop\\proyecto\\data\\all-20181021'

copy('C:\\Users\\Jonatan\\Desktop\\proyecto\\data\\corpus-final-jona-fix-naty2\\corpus-jona',
     dest_path)

copy('C:\\Users\\Jonatan\\Desktop\\proyecto\\data\\corpus-naty-20180720',
     dest_path)

copy('C:\\Users\\Jonatan\\Desktop\\proyecto\\data\\corpus-final-colo-fix',
     dest_path)
