import random
from os import listdir, mkdir
import shutil


def create(source_path, dest_path):
    mkdir(dest_path + '\\' + 'train')
    mkdir(dest_path + '\\' + 'valid')
    mkdir(dest_path + '\\' + 'test')

    filenames = []
    for file in listdir(source_path):
        if file.endswith('.ann'):
            filenames.append(file.split('.')[0])

    random.shuffle(filenames)

    counter = 1
    for filename in filenames:
        ann_filename = filename + '.ann'
        post_filename = filename + '.txt'
        folder = 'train'

        if counter == 5:
            folder = 'valid'
        elif counter == 6:
            folder = 'test'
            counter = 0

        shutil.copy(source_path + '\\' + ann_filename, dest_path + '\\' + folder + '\\' + ann_filename)
        shutil.copy(source_path + '\\' + post_filename, dest_path + '\\' + folder + '\\' + post_filename)
        counter += 1


# C:\\Users\\Jonatan\\Desktop\\proyecto\\data\\corpus-final-jona-fix-naty\\all
# C:\\Users\\Jonatan\\Desktop\\proyecto\\codigo\\NeuroNER-master\\data\\corpus-jona-fix-naty\\en
create('C:\\Users\\Jonatan\\Desktop\\proyecto\\data\\corpus-final-jona-fix-naty\\all',
       'C:\\Users\\Jonatan\\Desktop\\proyecto\\codigo\\NeuroNER-master\\data\\corpus-jona-fix-naty\\en')