from database.post_store import BUGTRAQ, FULL_DISCLOSURE, EXPLOIT_DB
from database.post_store import count_seclist_posts, count_exploitdb_posts, get_posts_sample
from database.post_store import copy_chosen_samples, delete_chosen_from_posts


def post_random_extract_all():
    post_random_extract(BUGTRAQ)
    post_random_extract(FULL_DISCLOSURE)
    post_random_extract(EXPLOIT_DB)

    delete_chosen_from_posts()
    print('delete the selected posts!!!')


def post_random_extract(site):
    start = get_start_year(site)
    # iterate start, by years until 2017?
    for year in range(start, 2018):
        # - count post by year
        count = count_posts(site, year)
        # - calculate 10% of the count to use as sample
        sample_count = count // 10
        print(site, year, count, sample_count)

        # - get the samples
        samples = get_posts_sample(site, year, sample_count)

        copy_chosen_samples(samples)

        # iteration end

    print('do something')


def get_start_year(site):
    if site == BUGTRAQ:
        return 1993
    elif site == FULL_DISCLOSURE:
        return 2002
    else:
        return 1997


def count_posts(site, year):
    if site == EXPLOIT_DB:
        return count_exploitdb_posts(site, year)
    else:
        return count_seclist_posts(site, year)


# post_random_extract(BUGTRAQ)
# post_random_extract(EXPLOIT_DB)
# post_random_extract_all()
delete_chosen_from_posts()
