

var fs = require('fs');
var path = require('path');
var dir = __dirname + "/corpus-final/corpus";



files = fs.readdirSync(dir, 'utf8').sort();


var ids = [];

files.forEach(function(f) {
    if (f!= "") {
        var id = f.split(".")[0];
        if (ids.indexOf(id) == -1) {
            ids.push(id);
        }
    }

});
console.log(ids.length);


var subcorpus = [];


function createFile(dir2, f){
    fs.createReadStream(dir + "/" + f + ".txt").pipe(fs.createWriteStream(__dirname +  dir2 + f + ".txt"));
    fs.createReadStream(dir + "/" + f + ".ann").pipe(fs.createWriteStream(__dirname +  dir2 + f + ".ann"));
}
var c = 0;
ids.forEach(function(id){
    if ((c % 3) == 0) {
        createFile("/subcorpus/", id);
        subcorpus.push(id);
        
    }
    c++;
})

console.log(subcorpus.length);
