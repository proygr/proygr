

//var url = "mongodb://127.0.0.1:27017/vulnerabilidades";
var url = "mongodb://vulnerabilidades:vulnerabilidades2018!@sl-us-dal-9-portal.3.dblayer.com:18285/vulnerabilidades?ssl=true";

var MongoClient = require('mongodb').MongoClient;
var fs = require('fs');
var folder = __dirname + "/output";
//var folder = __dirname + "/bugtraq/output";
//var folder = __dirname + "/full_disclosure/output/2007";
console.log(folder);
MongoClient.connect(url, function(err, client) {
    console.log("Connected successfully to server");
    //var q = {    site:'exploit_db',    cve : {        $exists : true    }    ,    cve : {        $ne : ""    }    ,    date: {        $in:[            /^2014/i        ]    },    content: {      $regex : ".*update_info.*"        }    }
    var q = {    site:'exploit_db',    cve : {        $exists : true    }    ,    cve : {        $ne : ""    }    ,    date: {        $in:[            /^2016/i        ]    }  }
    //var q = {  site: {      $regex : ".*bugtraq.*"        } , date:   {      $regex : ".*2014.*"        } }
    //var q = {  site: {      $regex : ".*full_disclosure.*"        } , date:   {      $regex : ".*2007.*"        } }

    var db = client.db("vulnerabilidades");
    
    var collection = db.collection('posts');
    // Find some documents
    collection.find(q).limit(200).toArray(function(err, docs) {
        console.log("Found the following records");
        //console.log(docs);

        docs.forEach(function(d){
            var content = d.content;
            var title = d.title;
            content = content.replace(/\r\n/g, " ");
            content = content.replace(/\r/g, "");
            content = content.replace(/\n/g, " ");
            content = content.replace(/\t/g, " ");
            content = content.replace(/\s+/g, " ");
            fs.writeFile(folder + "/" + d._id + ".txt", title + " " + content, function(err) {
                console.log("The file was saved!");
            }); 
            fs.writeFile(folder + "/" + d._id + ".ann", "" , function(err) {
                console.log("The file was saved!");
            }); 
            
        });
    });
  });