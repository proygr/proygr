T1	PRODUCT 19 30	PartyGaming
T2	PRODUCT 31 41	PartyPoker
T3	PRODUCT 268 279	PartyGaming
T4	PRODUCT 280 290	PartyPoker
T5	REFERENCES 322 389	http://www.security-objectives.com/advisories/SECOBJADV-2008-03.txt
T6	PRODUCT 400 417	PartyPoker Client
T7	VERSION 432 435	121
T8	VERSION 436 439	120
T9	PROBLEMTYPE 568 575	CWE-346
T10	RISK_LEVEL 649 655	Medium
T11	DIFFICULTY 668 676	Moderate
T12	REFERENCES 689 702	CVE-2008-3324
T13	DESCRIPTION 714 2691	PartyPoker.com (www.PartyPoker.com) is the world's largest online poker brand in terms of number of players and revenues. You'll find a great variety of poker games and tournaments, plus blackjack. SUMMARY The PartyGaming PartyPoker client program can be forced into downloading a malicious update. This is a result of the PartyPoker client not properly confirming the authenticity of the network update server or the executable update files themselves. When downloading an update, first the client program resolves the DNS address of the update host. Next, it establishes a TCP connection on port 80 of the previously resolved IP address. Then, it sends an HTTP request for an EXE file under the web server's Downloads directory. Upon receiving the HTTP response, the requested portable executable is written to disk and executed. ANALYSIS To successfully exploit this vulnerability an attacker must be able to somehow position themself such that they can impersonate the update server. This can be accomplished through DNS cache poisoning, ARP redirection, TCP hijacking, impersonation of a Wi-Fi Access Point, etc. The attacker also would have configured a rogue web server to push out update code of their choosing. Before PartyPoker downloads the update it communicates with another PartyGaming server in the 88.81.154.0/24 subnetwork via SSL to determine if a new client update is available; if so, a HTTP GET request is sent to www1.partypoker.com for an EXE file in the /Downloads/en/vcc directory and is stored on the local filesystem under C:\Programs\PartyGaming\tmpUpgrade and executed. Afterwards, the user may login and operate the PartyPoker client as usual. Since the update itself is downloaded from a seperate server, the client can contact the legitimate PartyGaming server during exploitation to determine if an update is available as normal. The attacker only needs to masquerade as www1.partypoker.com. WORKAROUND Do not use the PartyPoker client program.
T14	PROBLEMTYPE 543 566	Origin Validation Error
