T1	PRODUCT 0 4	Exim
T2	VERSION 5 13	< 4.86.2
T3	PROBLEMTYPE 16 42	Local Privilege Escalation
T4	RISK_LEVEL 167 171	High
T5	RISK_LEVEL 172 180	Critical
T6	PRODUCT 270 274	Exim
T7	VERSION 275 283	< 4.86.2
T8	PROBLEMTYPE 295 315	Privilege Escalation
T9	DESCRIPTION 1165 3751	The vulnerability stems from Exim in versions below 4.86.2 not performing sanitization of the environment before loading a perl script defined with perl_startup setting in exim config. perl_startup is usually used to load various helper scripts such as mail filters, gray listing scripts, mail virus scanners etc. For the option to be supported, exim must have been compiled with Perl support, which can be verified with: [dawid@centos7 ~]$ exim -bV -v | grep i Perl Support for: crypteq iconv() IPv6 PAM Perl Expand_dlfunc TCPwrappers OpenSSL Content_Scanning DKIM Old_Demime PRDR OCSP To perform the attack, attacker can take advantage of the exim's sendmail interface which links to an exim binary that has an SUID bit set on it by default as we can see below: [dawid@centos7 ~]$ ls -l /usr/sbin/sendmail.exim lrwxrwxrwx. 1 root root 4 Nov 30 00:45 /usr/sbin/sendmail.exim -> exim [dawid@centos7 ~]$ ls -l /usr/sbin/exim -rwsr-xr-x. 1 root root 1222416 Dec 7 2015 /usr/sbin/exim Normally, when exim sendmail interface starts up, it drops its root privileges before giving control to the user (i.e entering mail contents for sending etc), however an attacker can make use of the following command line parameter which is available to all users: -ps This option applies when an embedded Perl interpreter is linked with Exim. It overrides the setting of the perl_at_start option, forcing the starting of the interpreter to occur as soon as Exim is started. As we can see from the documentation at: http://www.exim.org/exim-html-current/doc/html/spec_html/ch-embedded_perl.html the perl_at_start option does the following: "Setting perl_at_start (a boolean option) in the configuration requests a startup when Exim is entered." Therefore it is possible to force the execution of the perl_startup script defined in the Exim's main config before exim drops its root privileges. To exploit this setting and gain the effective root privilege of the SUID binary, attackers can inject PERL5OPT perl environment variable, which does not get cleaned by affected versions of Exim. As per perl documentation, the environment variable allows to set perl command-line options (switches). Switches in this variable are treated as if they were on every Perl command line. There are several interesting perl switches that that could be set by attackers to trigger code execution. One of these is -d switch which forces perl to enter an interactive debug mode in which it is possible to take control of the perl application. An example proof of concept exploit using the -d switch can be found below. V.
T10	PRODUCT 5714 5718	Exim
T11	REFERENCES 5849 5924	http://legalhackers.com/advisories/Exim-Local-Root-Privilege-Escalation.txt
T12	REFERENCES 5925 5945	http://www.exim.org/
T13	REFERENCES 5946 5994	http://www.exim.org/static/doc/CVE-2016-1531.txt
T14	REFERENCES 5995 6073	http://www.exim.org/exim-html-current/doc/html/spec_html/ch-embedded_perl.html
T15	REFERENCES 6074 6150	https://github.com/Exim/exim/commit/29f9808015576a9a1f391f4c6b80c7c606a4d99f
T16	REFERENCES 6151 6164	CVE-2016-1531
T17	REFERENCES 6165 6225	https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-1531
T18	PRODUCT 5218 5222	Exim
T19	PRODUCT 5269 5273	Exim
T21	PRODUCT 5320 5324	Exim
T23	PRODUCT 366 370	Exim
T24	PRODUCT 774 778	Exim
T25	PRODUCT 848 852	Exim
T22	DESCRIPTION 939 1122	When Exim installation has been compiled with Perl support and contains a perl_startup configuration variable it can be exploited by malicious local attackers to gain root privileges.
T26	DESCRIPTION 4991 5169	This vulnerability could be exploited by attackers who have local access to the system to escalate their privileges to root which would allow them to fully compromise the system.
T27	PRODUCT 3956 3960	Exim
T28	PRODUCT 4129 4133	exim
