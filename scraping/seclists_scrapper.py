import requests
from lxml import html
from datetime import datetime
# from database.post_store import save_link, mark_error_link
# from database.post_store import save_post, add_comment_to_post, get_links, mark_link
from database.storage import save_link, get_links, mark_error_link, save_post_data, mark_link
from bs4 import BeautifulSoup, Comment, NavigableString
import re
import sys
import traceback


BUGTRAQ = 'bugtraq'
FULL_DISCLOSURE = 'full_disclosure'
SECLISTS_BASE_LINK = 'http://seclists.org'
MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
MONTHS_DIC = {
    'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6,
    'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10, 'Nov': 11, 'Dec': 12
}
BUGTRAQ_YEARS = {
    'start': 1993,
    'end': 2017
}
FULL_DISCLOSURE_YEARS = {
    'start': 2002,
    'end': 2017
}


def scrap_bugtraq_post_links(from_date):
    scrap_post_links(SECLISTS_BASE_LINK + '/bugtraq/', BUGTRAQ, from_date)


def scrap_full_disclosure_post_links(from_date):
    scrap_post_links(SECLISTS_BASE_LINK + '/fulldisclosure/', FULL_DISCLOSURE, from_date)


def scrap_post_links(site_link, site_name, from_date):
    main_page = requests.get(site_link)
    main_page_tree = html.fromstring(main_page.content)

    # class=”calendar”
    main_page_table_rows = main_page_tree.xpath("//table[@class='calendar']/*")
    main_page_table_header = main_page_table_rows.pop(0)
    for elem_row in main_page_table_rows:
        report_year = elem_row[0].text
        index = 0
        for elem_col in elem_row:
            # filter first column
            if report_year != elem_col.text:
                link = elem_col.xpath("a")
                if not link:
                    link = elem_col.xpath("strong/a")

                if link:
                    # href looks 'bugtraq/1993/Nov/index.html'
                    full_link = SECLISTS_BASE_LINK + link[0].get('href')
                    report_month = main_page_table_header[index].text
                    # print(report_year, main_page_table_header[index].text, full_link)
                    if int(report_year) > from_date.year \
                            or (int(report_year) == from_date.year and MONTHS_DIC[report_month] >= from_date.month):
                        scrap_year_month_page(full_link, site_name, report_year, report_month)

            index += 1


# link example: http://seclists.org/bugtraq/2017/Jan/index.html
def scrap_year_month_page(link, site_name, year=None, month=None):
    page = requests.get(link)
    page_tree = html.fromstring(page.content)
    page_html_lists = page_tree.xpath('//li')

    selected_html_links = []
    for candidate_html_list_item in page_html_lists:
        link_ref = candidate_html_list_item.xpath('a[@name and @href and string-length(@name)>0 and @name=@href]')
        if link_ref:
            selected_html_links.extend(link_ref)

    if selected_html_links:
        for link_item in selected_html_links:
            url_to_thread = discard_last_url_part(link) + '/' + link_item.get('href')
            link_filter = {
                'site': site_name,
                'link': url_to_thread,
            }
            link_info = {
                'site': site_name,
                'year': year,
                'month': month,
                'link': url_to_thread
            }
            save_link(link_filter, link_info)


def download_posts(site, site_years, from_date, logger, downloaded=None, error=None):
    current_date = datetime.now()
    for year in range(site_years['start'], current_date.year + 1):
        for month in MONTHS_DIC.keys():
            # check if we have to download according to the date
            if year > from_date.year or (year == from_date.year and MONTHS_DIC[month] >= from_date.month):
                link_info = {
                    'site': site,
                    'year': str(year),
                    'month': month
                }
                links = get_links(link_info, downloaded, error)
                for report_link in links:
                    try:
                        # print(site, year, month, report_link['link'])
                        scrap_report_bs(site, report_link['link'], logger)
                    except SystemExit:
                        print('last report link:', report_link)
                        sys.exit()
                    except:
                        link_info = {
                            'site': site,
                            'link': report_link['link']
                        }
                        mark_error_link(link_info, traceback.format_exc())

                logger('download for year ' + str(year) + ' and month ' + month + ' finished')
                # print('download for year', year, ' and month', month, ' finished')


def download_bugtraq_posts(from_date, logger, downloaded=None, error=None):
    download_posts(BUGTRAQ, BUGTRAQ_YEARS, from_date, logger, downloaded, error)


def download_full_disclosure_posts(from_date, logger, downloaded=None, error=None):
    download_posts(FULL_DISCLOSURE, FULL_DISCLOSURE_YEARS, from_date, logger, downloaded, error)


def scrap_report_bs(site, report_link, logger):
    report_page = requests.get(report_link)

    if report_page.status_code == 403:
        # Web Abuse!!!
        # print('Blocked for possible web abuse, get another IP or come later')
        logger('Blocked for possible web abuse, get another IP or come later')
        sys.exit()

    soup = BeautifulSoup(report_page.text, 'html5lib')
    # print('encoding:', report_page.encoding)

    # title_element = report_page_tree.xpath("//comment()[. = 'X-Subject-Header-Begin']/following-sibling::font/b")
    # report_title = title_element[0].text
    title_element = soup.find(
        text= lambda text: isinstance(text, Comment) and text.string == 'X-Subject-Header-Begin')
    report_title = title_element.find_next_sibling('font').b.string
    # print('report_title:', report_title)

    from_previous_element = soup.find(
        text=lambda text: isinstance(text, Comment) and text.string == 'X-Head-of-Message')
    post_from_element = from_previous_element.find_next_sibling('em', text='From')
    post_from = post_from_element.next_sibling.string[2:]
    # print('post_from:', post_from)

    date_element = from_previous_element.find_next_sibling('em', text='Date')
    post_date = parse_string_date(date_element.next_sibling.string[2:])
    # print('post_date:', post_date)

    post_content = ''
    content_comment_begin = soup.find(
        text=lambda text: isinstance(text, Comment) and text.string == 'X-Body-of-Message')
    content_element = content_comment_begin.next_sibling
    # print(content_element)
    while content_element.string != 'X-Body-of-Message-End':
        if not isinstance(content_element, NavigableString):
            # print('***PARENT***', content_element.name)
            # only process "pre" blocks, this is to avoid quoteblock
            if content_element.name == 'pre':
                for child in content_element.children:
                    if child.name == 'a':
                        post_content += child['href']
                        # print('***HREF***', child['href'])
                    else:
                        # print('***CHILD***', child.name, 'string:', child.string)
                        if child.string is not None:
                            post_content += child.string

        content_element = content_element.next_sibling
        # print(content_element)

    # empty content, maybe the information is an attached file
    if post_content.isspace():
        attachment_element = soup.find('a', href=re.compile('att-'))
        if attachment_element:
            attachment_link = discard_last_url_part(report_link) + '/' + attachment_element['href']
            attachment_page = requests.get(attachment_link)
            post_content = attachment_page.content.decode('utf-8')
        else:
            link_info = {
                'site': site,
                'link': report_link
            }
            mark_error_link(link_info, 'not possible to obtain content')
            # print('error!!!, no content read and no attachment found')
            logger('error!!!, no content read and no attachment found')

    post_filter = {
        'site': site,
        'link': report_link
    }

    comment_result = is_comment(soup)
    if comment_result['comment']:
        url_to_parent = discard_last_url_part(report_link) + '/' + comment_result['parent']
        logger('post is a comment, is going to be ignored!')
        # print('parent:', url_to_parent)
        # add_comment_to_post(site, report_link, report_title, post_from, post_date, post_content, url_to_parent)
    else:
        post_info = {
            'site': site,
            'link': report_link,
            'title': report_title,
            'from': post_from,
            'date': post_date,
            'comments': [],
            'content': post_content
        }
        save_post_data(post_filter, post_info)
        # print('save_post')

    # mark link as downloaded
    mark_link(post_filter)

    # print('**** POST CONTENT ****', post_content.isspace())
    # print('post_content')


def is_comment(soup):
    current_thread_comment_begin = soup.find(
        text=lambda text: isinstance(text, Comment) and text.string == 'X-BotPNI')
    thread_list_element = current_thread_comment_begin.find_next_sibling('ul')
    list_items = thread_list_element.find_all(['a', 'strong'])
    result = {
        'comment': list_items[0].name == 'a',

    }
    if list_items[0].name == 'a':
        return {
            'comment': True,
            'parent': list_items[0]['href']
        }
    else:
        return {
            'comment': False,
        }


# example
# input: http://seclists.org/bugtraq/2017/Jan/index.html
# output: http://seclists.org/bugtraq/2017/Jan
def discard_last_url_part(url):
    splitted = url.split('/')
    size = len(splitted)
    splitted.pop(size - 1)
    new_url = '/'.join(splitted)
    return new_url


def parse_string_date(str_date):
    if str_date:
        # don't use the timezone parameters
        partial_date = ' '.join(str_date.split(' ')[0:5])
        date_pattern = '%a, %d %b %Y %H:%M:%S'
        try:
            return datetime.strptime(partial_date, date_pattern)
        except ValueError:
            return partial_date

    return str_date


def scrap_site_bugtraq(from_date, logger=None):
    if logger is None:
        logger = print

    logger('Starting Bugtraq scrapper')

    # first start scrapping all possible links
    logger('getting all links available to download')
    scrap_bugtraq_post_links(from_date)
    logger('all links saved!')

    # download posts from the given date
    logger('downloading posts from ' + from_date.strftime('%d/%m/%Y'))
    download_bugtraq_posts(from_date, logger)
    logger('download finished!')

    logger('Bugtraq scrapper finished successfully')


def scrap_site_fulld(from_date, logger=None):
    if logger is None:
        logger = print

    logger('Starting Full Disclosure scrapper')

    # first start scrapping all possible links
    logger('getting all links available to download')
    scrap_full_disclosure_post_links(from_date)
    logger('all links saved!')

    # download posts from the given date
    logger('downloading posts from ' + from_date.strftime('%d/%m/%Y'))
    download_full_disclosure_posts(from_date, logger)
    logger('download finished!')

    logger('Full Disclosure scrapper finished successfully')


# scrap_bugtraq_post_links()
# scrap_full_disclosure_post_links()
# scrap_year_month_page('http://seclists.org/bugtraq/2016/Jan/index.html', '2016', 'Jan')

# http://seclists.org/fulldisclosure/2014/Mar/index.html
# scrap_year_month_page('http://seclists.org/fulldisclosure/2014/Mar/index.html', '2014', 'Mar')
# scrap_report_bs(BUGTRAQ, 'http://seclists.org/bugtraq/2016/Mar/103')
# scrap_report_bs(BUGTRAQ, 'http://seclists.org/bugtraq/2016/Jan/21')

# scrap_report_bs(BUGTRAQ, 'http://seclists.org/bugtraq/2012/Aug/140')
# scrap_report_bs(BUGTRAQ, 'http://seclists.org/bugtraq/2016/Jan/21')

# download_bugtraq_posts(error=True)
# download_full_disclosure_posts(error=True)
